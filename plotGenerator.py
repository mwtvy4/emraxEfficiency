__author__ = 'mwt'
import csv
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
#plotly.offline.init_notebook_mode()

matrix = []
scale = 100
maxRPM = 4999
rpmStep = -100
maxTorque = 240
torqueStep = -1
for y in range(0,maxRPM,10): #x is row of torques
    #rpm = maxRPM - x # count backwards
    rpm =  y
    row = []
    for x in range(0,maxTorque,1): # y is torque
    #     for y in range(maxTorque,0,torqueStep): # y is torque
        #torque = maxTorque -y
        torque = x
        #print(y, " and " , torque)
        for z in [1]: #z is efficiency
            with open('emrax.csv') as emrxEff:
                reader = csv.reader(emrxEff)
                eff = list(reader)
                Tfactor = int(torque/25)
                Rfactor = int(rpm/250)

                lowRpm = Rfactor*250
                highRpm = (Rfactor + 1) * 250
                lowTorque = (Tfactor) * 25
                highTorque = (Tfactor + 1) * 25

            #one for each quadrant
                TRfactor = (rpm - lowRpm) * (highTorque - torque) / (25 * 250)
                TLfactor = (highRpm - rpm) * (highTorque - torque) / (25 * 250)
                BRfactor = (rpm - lowRpm) * (torque - lowTorque) / (25 * 250)
                BLfactor = (highRpm - rpm) * (torque - lowTorque) / (25 * 250)

                loss = (((TLfactor * float(eff[Rfactor][Tfactor])) + (TRfactor * float(eff[Rfactor + 1][Tfactor])) + (BRfactor * float(eff[Rfactor + 1][Tfactor + 1])) + (BLfactor * float(eff[Rfactor][Tfactor + 1]))))
                row.append(loss)
    #with open('plot.csv','a', newline='') as csvoutput:
    #        writer = csv.writer(csvoutput, delimiter=',',quotechar='|')
    #        writer.writerow(row)
    matrix.append(row)
#print(matrix)
data = [
    go.Surface(
        z=matrix
    )
]
layout = go.Layout(
    title='Emrax 228 efficiency',
    autosize=True,
    width=1000,
    height=700,
    xaxis=dict(
        title='x axis',
        mirror=True,
        backgroundcolor="rgba(104,204,204,0.5)",
        showgrid=False),
    yaxis=dict(mirror=False),
    margin=dict(
        l=65,
        r=50,
        b=65,
        t=90
    )
)
fig = go.Figure(data=data, layout=layout)
plotly.offline.plot(fig, filename='efficiency.html')