__author__ = 'mwt'
import csv

fileName = 'emrax.csv'
targetFile = 'addedDrag.csv' # this is the file to append to



with open(fileName) as emrxEff:
    reader = csv.reader(emrxEff)
    eff = list(reader)

with open(targetFile) as data:
    effArgs = csv.DictReader(data,fieldnames=['rpm','torque','power'])
    for row in effArgs:
        power = float(row['power'])

        torque = float(row['torque'])
        Tfactor = int(torque/25)

        rpm = float(row['rpm'])
        Rfactor = int(rpm/250)

        lowRpm = Rfactor*250
        highRpm = (Rfactor + 1) * 250
        lowTorque = (Tfactor) * 25
        highTorque = (Tfactor + 1) * 25

        #one for each quadrant
        TRfactor = (rpm - lowRpm) * (highTorque - torque) / (25 * 250)
        TLfactor = (highRpm - rpm) * (highTorque - torque) / (25 * 250)
        BRfactor = (rpm - lowRpm) * (torque - lowTorque) / (25 * 250)
        BLfactor = (highRpm - rpm) * (torque - lowTorque) / (25 * 250)

        loss = (power  * (100-((TLfactor * float(eff[Rfactor][Tfactor])) + (TRfactor * float(eff[Rfactor + 1][Tfactor])) + (BRfactor * float(eff[Rfactor + 1][Tfactor + 1])) + (BLfactor * float(eff[Rfactor][Tfactor + 1]))))/100)

        with open('output.csv','a', newline='') as csvoutput:
            writer = csv.writer(csvoutput, delimiter=',',quotechar='|')
            writer.writerow([loss])

